function validarUsuari(){
    var msg="";
    document.usuari.validar.value='0';
    
    if (document.usuari.nom.value==''){
        msg="Falta el nom.\n";
        document.usuari.validar.value='1';
    }
    
    if (document.usuari.cognoms.value==''){
        msg+="Falta els cognoms.\n";
        document.usuari.validar.value='1';
    }
    if (document.usuari.email1.value==''){
        msg+="Falta l'email.\n";
        document.usuari.validar.value='1';
    }
    if (document.usuari.email2.value==''){
        msg+="Falta la validacio del email.\n";
        document.usuari.validar.value='1';
    }
    if (document.usuari.email1.value!='' && document.registre.email2.value!='' && document.registre.email1.value!=document.registre.email2.value){
        msg+="Els emails no coincideixen.\n";
        document.usuari.validar.value='1';
    }
    if (document.usuari.passwd1.value==''){
        msg+="Falta la contrasenya.\n";
        document.usuari.validar.value='1';
    }
    if (document.usuari.passwd2.value==''){
        msg+="Falta la validacio de contrasenya.\n";
        document.usuari.validar.value='1';
    }
    if (document.usuari.passwd1.value!='' && document.registre.passwd2.value!='' && document.registre.passwd1.value!=document.registre.passwd2.value){
        msg+="Les contrasenyes no coincideixen.\n";
        document.usuari.validar.value='1';
    }
    if (document.usuari.passwd2.value==''){
        msg+="Falta la validacio de contrasenya.\n";
        document.usuari.validar.value='1';
    }
    
    if(document.usuari.validar.value=='0'){document.usuari.submit;}else{alert msg;}
}