<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BaseDatos
 *
 * @author alan
 */
class BaseDatos {
   private $_usuari="root";
    private $_passwd="";
    private $_servidor="localhost";
    private $_basedades="foro";
    private $_resultat;
    private $_conection = null;
   private $_resource  = null;
   
   public function __construct()
   {
      $this->_conection = mysql_connect($this->_servidor,$this->_usuari,$this->_passwd) or die('no se realizo la conexion');
   }
   
   public function select_db()
   {
      if(is_null($this->_conection)) {return false;}
      mysql_select_db($this->_basedades,$this->_conection);
      return true;
   }
   
   public function query($query,$datos = array())
   {
      if(!empty($datos))
      {
         $tmp = new ArrayObject();
         foreach($datos as $valor)
         {
            $tmp->append(mysql_real_escape_string($valor,$this->_conection));
         }
         $query = vsprintf($query,$tmp);
         unset($tmp,$datos);
      }
      
         if(is_null($this->_conection)) {return false;}
         $this->resource = mysql_query($query, $this->_conection);
         return true;
   }
   
   public function fetch_assoc()
   {
      if(is_null($this->_resource)) {return false;}
      return mysql_fetch_assoc($this->_resource);
   }
}
/*Exemple us
 include("mysql.php");

$db = new MySQL();
$consulta = $db->consulta("SELECT id FROM mitabla1");
if($db->num_rows($consulta)>0){
  while($resultados = $db->fetch_array($consulta)){ 
   echo "ID: ".$resultados['id']."<br />"; 
 }
}
 */
?>
